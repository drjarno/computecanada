#include <iostream>

struct vec_t {
  double x, y;
};

double *position_grid = nullptr;
double *velocity_grid = nullptr;

void setup(vec_t dimensions, vec_t stepsize, bool estimate) {
  unsigned long memory_allocated = 0;

  size_t resx = dimensions.x / stepsize.x + 1;
  size_t resy = dimensions.y / stepsize.y + 1;

  if(!estimate) {
    // Allocate memory
    position_grid = new double[resx*resy];

    // Initialize array
    for(int i = 0; i < resx*resy; i++)
      position_grid[i] = 0.;
  }
  memory_allocated += resx*resy*sizeof(double);

  if(!estimate) {
    // Allocate memory
    velocity_grid = new double[resx*resy];

    // Initialize array
    for(int i = 0; i < resx*resy; i++)
      velocity_grid[i] = 0.;
  }
  memory_allocated += resx*resy*sizeof(double);

  std::cout << "Need " << memory_allocated / (1024.*1024.*1024.) << " GiB" << std::endl;
}

void simulate() {
  // The actual simulation would go here
  std::cout << "Starting simulation" << std::endl;
}

void cleanup() {
  // Freeing the allocated memory
  std::cout << "Cleaning up" << std::endl;
  delete[] position_grid;
  delete[] velocity_grid;
}

int main(int argc, char *argv[]) {
  bool estimate = false;

  // Check the command-line argument to see if we are only estimating
  if(argc == 2 && std::string(argv[1]) == "--estimate")
    estimate = true;

  // Set the parameters for the simulation
  vec_t dimensions = {100., 100.};
  vec_t stepsize = {0.01, 0.01};
  
  setup(dimensions, stepsize, estimate);
  if(estimate)
    return 0;

  simulate();
  cleanup();

  return 0;
}
