#!/bin/bash -l
#SBATCH --job-name=python_test
#SBATCH --account=def-jarno #adjust this to match the accounting group you are using to submit jobs
#SBATCH --time=0-00:05     #adjust this to match the walltime of your job
#SBATCH --ntasks=1
#SBATCH --mem=4000         #adjust this according to your the memory requirement per node you need
#SBATCH --mail-user=jvanderk@uottawa.ca #adjust this to match your email address
#SBATCH --mail-type=ALL

#Load the appropriate modules
module load python/3.7.0

#Run the program
python pythontest.py
