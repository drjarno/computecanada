#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct vec_t {
  double x, y;
};

int main() {
  const int num_particles = 100;
  const int num_steps = 200;
  const int checkpoint_every = 10;
  const double force = 0.001;
  int current_step = 0;
  
  cout << "Simulation with " << num_particles << " particles" << endl;

  vec_t *positions = new vec_t[num_particles];
  ifstream datastream("run.ckp");
  if(datastream) {
    // If a checkpoint file exists, continue from it
    cout << "Existing checkpoint was found." << endl;
    datastream.read((char *)&current_step, sizeof(int));
    datastream.read((char *)positions, num_particles*sizeof(vec_t));
    datastream.close();
    cout << "Continuing from step " << current_step << '.' << endl;
  }
  else {
    // No checkpoint file exists, so setup the initial parameters
    cout << "Initializing particle positions." << endl;
    for(int i = 0; i < num_particles; i++) {
      positions[i].x = i / 10;
      positions[i].y = i % 10;
    }
  }

  // Run simulation
  for(; current_step < num_steps; current_step++) {
    // Do the physics
    for(int i = 0; i < num_particles; i++) {
      for(int j = 0; j < num_particles; j++) {
        if(i == j)
          continue;
        double dist2 = (positions[i].x - positions[j].x)*(positions[i].x - positions[j].x) + (positions[i].y - positions[j].y)*(positions[i].y - positions[j].y);
        positions[i].x -= force*(positions[i].x - positions[j].x)/dist2;
        positions[i].y -= force*(positions[i].y - positions[j].y)/dist2;
      }
    }

    cout << current_step << "/" << num_steps << endl;
    
    // Write data to file
    ofstream posfile(string("positions_") + to_string(current_step) + string(".dat"));
    for(int i = 0; i < num_particles; i++)
      posfile << positions[i].x << ", " << positions[i].y << '\n';

    // Create a checkpoint and exit
    if((current_step + 1) % checkpoint_every == 0) {
      current_step++;
      cout << "Saving checkpoint." << endl;
      ofstream checkpointfile("run.ckp");
      checkpointfile.write((char *)&current_step, sizeof(int));
      checkpointfile.write((char *)positions, num_particles*sizeof(vec_t));
      checkpointfile.close();
      return 0;
    }
  }

  cout << "Done" << endl;

  return 0;
}
