#/usr/bin/env python

import sys

print(sys.version + "\n")

total = 0
for i in range(1000):
  total = total + i*i

print(f"Total: {total}")

with open("test.txt", "w") as f:
  f.write(f"Total: {total}\n")

sys.exit(0)
