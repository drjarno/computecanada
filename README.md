[français](#markdown-header-seminaire-calcul-canada) | [english](#markdown-header-compute-canada-seminar)
# Séminaire calcul canada

Ce sont les fichier demo qui démontrent l'utilisation de calcul canada.

## Soumettre des tâches
Pour soumettre des calculs sur calcul canada, vous devriez les soumettre comme « tâches ». Ils sont soumis à l'ordonnanceur qui mise la demande dans une file d'attente jusqu'à ce qu'un espace soit disponible. En général, plus les ressources sont demandes, plus vous attendrez.

A titre d'exemple, nous utilons un programme Python simple qu'on a appellé « pythontest.py" ». Il doit avoir au moins version 3.7.0 de Python, parce que le programme utilise un châine avec « f"" ». Pour lancer le programme, normallement vous tapiez

    python pythontest.py

Cela ne fonctionne pas, si votre version de Python est trop ancienne.

Pour soumettre à calcul canada, vous devriez utiliser un script shell appelé « submitpy.sh ». Pour ce faire, connectez vous dans calcul canada et tapez

    sbatch submitpy.sh

Veuillez consulter [docs.computecanada.ca](https://docs.computecanada.ca) pour obtenir la documentation complète.
## estimation de mémoire
La possibility d'estimer la quantité mémoire requise est très utile lorsque vous essayez déterminer combien de mémoire vous devriez demander à l'ordonnanceur.

Pour compilez, tapez

    g++ memmeasure.cpp -o memmeasure -std=c++11

Pour exécuter le programme et juste estimer la quantité de mémoire, tapez

    ./memmeasure --estimate

Vous verrez le programma a besoin d'environ 1.5 Go. Maintenent, essayez de exécuter sans estimer

    ./memmeasure

Cette fois, le programme affecta le mémoire et commenca la simulation.

## Points de contrôle
Des points de contrôle sont un technique pour continuer des simulations des données enregistrées. Ils permettent  récupération après panne, mais ils sont aussi utiles quand on exécutant des simulations plus longues parce que plusieurs tâches courtes attendent moins longtemps dans la liste d'attente que une tâches longue.

Pour compilez, tapez

    g++ checkpointing.cpp -o checkpointing -std=c++11

Pour exécuter le programme, tapez

    ./checkpointing

Éxecutes plusieurs de temps pour voir les points de contrôle e
# Compute Canada seminar

These are demo files demonstrating the use of Compute Canada.

## Submitting jobs
To run calculations on Compute Canada, you need to submit them as "jobs". These are submitted to the scheduler which puts your job in the queue until space is available. The more resources you request, the longer you wait.

As a sample calculation, we use a simple Python program called "pythontest.py". It has needs at least Python 3.7.0 to run because it uses a f"" string. To run it, normally you would type

    python pythontest.py

This will fail if your Python version is too old.

To submit to Compute Canada, you use a shell script called "submitpy.sh". Submit on Compute Canada by logging in and typing

    sbatch submitpy.sh

See [docs.computecanada.ca](https://docs.computecanada.ca) for the full documentation.
## Memory estimation
Having the ability to estimate the required memory is very helpful when trying to determine how much memory to ask for.

To compile, type

    g++ memmeasure.cpp -o memmeasure -std=c++11

To run and just estimate the amount of memory needed, type

    ./memmeasure --estimate

You'll see it will need about 1.5 GB. Now try to run it without estimating

    ./memmeasure

Now the program will actually allocate the memory and start the simulation.

## Checkpointing
Checkpointing is a technique for continuing simulations from saved data. This enables recovery from crashes and is also useful when running long simulations as many smaller jobs go through the queue faster than one large job.

To compile, type

    g++ checkpointing.cpp -o checkpointing -std=c++11

To run, type

    ./checkpointing

Run it multiple times to see it continuing from where it left off.
